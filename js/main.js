function processCSV(data) {

  const rows = data.split(/\r\n|\n/)
  const heading = rows.shift().split(';')
  return rows.filter((row) => row).map((row) => {
    const columns = row.split(';')
    const result = {}
    heading.map((h, idx) => {
      result[h] = columns[idx]
    })
    return result
  })
}

$(function () {

  /*$('#about_carousel').owlCarousel({
    loop: false,
    margin: 10,
    nav: false,
    items: 1    
  })*/

  // Set the date we're counting down to
  var countDownDate = new Date("September 21, 2021 00:00:00").getTime();

  // Update the count down every 1 second
  var x = setInterval(function () {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24)).toString().padStart(3, "0");
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)).toString().padStart(2, "0");;
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)).toString().padStart(2, "0");;
    var seconds = Math.floor((distance % (1000 * 60)) / 1000).toString().padStart(2, "0");;

    // Output the result in an element with id="demo"
    document.getElementById("timer").innerHTML = "<div class='start-in'>Começa em:</div>" +
      days + "&nbsp;<span>dias</span> " +
      hours + "&nbsp;<span class='mr-1'>h</span>:" +
      minutes + "&nbsp;<span class='mr-1'>m</span>:" +
      seconds + "&nbsp;<span>s</span>";

    // If the count down is over, write some text 
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("timer").innerHTML = "EXPIRED";
    }
  }, 1000);

})