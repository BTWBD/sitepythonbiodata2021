$(function () {

  $("a.edition").click(function() {
    $("a.edition").removeClass("active");
    $(this).addClass("active");
 });

  // LOAD TEAM
  $.ajax({
    type: 'GET',
    url: 'data/team.csv',
    dataType: "text",
    async: true,
    success: (result) => {
      var data = processCSV(result)
      var carousel = $('#ajax_team')
      data.map(member => {
        const { nome, instituicao, foto, titulacao, formacao } = member
        const item = `<div class="image-flip" >
                    <div class="mainflip flip-0">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p class="text-center"><img class=" img-fluid" src="${foto}" alt="card image"></p>
                                    <h4 class="card-title">${nome}</h4>
                                    <p class="card-text">${titulacao}</p>                                    
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h4 class="card-title">${nome}</h4>
                                    <p class="card-text">${formacao}</p>
                                    <p class="card-text">${instituicao}</p>                                                                                                         
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`
        carousel.append(item)
      })

      $('#ajax_team').owlCarousel({
        items: 4,
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        smartSpeed: 200,
        responsive: {
          1: {
            items: 1
          },
          800: {
            items: 2
          },
          1000: {
            items: 4
          }
        }
      })
    }
  });

  // LOAD DATE
  $.ajax({
    type: 'GET',
    url: 'data/dates.csv',
    dataType: "text",
    async: true,
    success: (result) => {
      var data = processCSV(result);
      var table = $('#ajax_dates')

      theader = (content) => `<thead class="thead-light" > <tr>${content}</tr></thead> `
      tbody = (content) => `<tbody> ${content}</tbody>`
      tr = (content) => `<tr>${content}</tr>`

      var headers = Object.keys(data[0])
      var dateRows = data.map(d => headers.map(h => `<td>${d[h]}</td> `).join("\n"))

      table.append(theader(headers.map(h => `<th class="head" scope = "col" > ${h}</th> `).join(' ')))
      table.append(tbody(dateRows.map(r => tr(r)).join(' ')))
    }
  });

  // LOAD SCHEDULE
  $.ajax({
    type: 'GET',
    url: 'data/schedule.csv',
    dataType: "text",
    async: true,
    success: (result) => {
      var data = processCSV(result);

      var buttons = $("#ajax_schedule_days")
      var carousel = $('#ajax_schedule')

      var days = [...new Set(data.map(d => d.dia))]

      var schedule = Object.fromEntries(days.map(key => [key, []]));

      data.map(activity => {
        const { dia, horario, atividade } = activity
        schedule[dia].push({ horario, atividade })
      })

      card = (idx, content) => `<div class="card" style="border: none;" data-hash="schedule${idx}">
        <div class="card-body pb-0">
        <table class="table table-bordered">${content}</table>
        </div>
        </div>`
      theader = `<thead class=" thead-light"><tr><th class="head" scope="col">Horário</th><th class="head" scope="col">Atividade</th></tr></thead>`
      tbody = (content) => `<tbody>${content}</tbody>`
      tr = (content) => `<tr>${content}</tr>`

      var active = true
      days.map((dia, idx) => {

        const tds = schedule[dia].map(({ horario, atividade }) =>
          tr(`<td>${horario}</td><td>${atividade}</td>`)
        ).join(' ')
        const tableContent = theader + tbody(tds)
        const item = card(idx, tableContent)
        carousel.append(item)

        var btn = `<div class="col"><a class="btn schedule btn-light btn-block" href="#schedule${idx}">${dia}</a></div>`
        if (active) {
          btn = `<div class="col"><a class="active schedule btn btn-light btn-block" href="#schedule${idx}">${dia}</a></div>`
          active = false
        }
        buttons.append(btn)
      })

      $("a.schedule").click(function() {
        $("a.schedule").removeClass("active");
        $(this).addClass("active");
     });

      $('#ajax_schedule').owlCarousel({
        loop: false,
        margin: 10,
        nav: false,
        dots: false,
        items: 1
      })
    }
  })

  // LOAD LECTURES
  $.ajax({
    type: 'GET',
    url: 'data/lectures.csv',
    dataType: "text",
    async: true,
    success: (result) => {
      var data = processCSV(result);

      var buttons = $("#ajax_lecture_days")
      var carousel = $('#ajax_lecture')

      var days = [...new Set(data.map(d => d.dia))]

      var lectures = Object.fromEntries(days.map(key => [key, []]));

      data.map(({ dia, titulo, palestrante, instituicao, horario, img }) =>
        lectures[dia].push({ titulo, palestrante, instituicao, horario, img })
      )

      card = (idx, content) => {
        let html = `<div class="card" style="border: none;" data-hash="lecture${idx}">
          <div class="card-body pb-0">
            <h3 class="card-title">${content.titulo}</h3>
            <div class="box">`
        html += content.img ? `<p><span class="image left">
                    <img style="width: 25%; float: left; padding: 0 1.5em 1em 0; top: 0.25em;"
                      src="${content.img}" alt=""></span></p>` : ""
        html += `<h4>Palestrante</h4>${content.palestrante}<br><br>
              <h4>Instituição</h4>${content.instituicao}<br><br>
              <h4 style="display:inline;">Horário:</h4>&nbsp;${content.horario}<br><br>
            </div>
          </div>
        </div>`
        return html
      }

      var active = true
      days.map((dia, idx) => {

        const item = lectures[dia].map(speakerData => card(idx, speakerData)).join(" ")
        carousel.append(item)

        var btn = `<div class="col"><a class="btn lecture btn-light btn-block" href="#lecture${idx}">${dia}</a></div>`
        if (active) {
          btn = `<div class="col"><a class="active lecture btn btn-light btn-block" href="#lecture${idx}">${dia}</a></div>`
          active = false
        }
        buttons.append(btn)        
      })

      $("a.lecture").click(function() {
        $("a.lecture").removeClass("active");
        $(this).addClass("active");
     });

      $('#ajax_lecture').owlCarousel({
        loop: false,
        margin: 10,
        nav: false,
        dots: false,
        items: 1
      })
    }
  })


});


/* CONTENT REDES SOCIAIS
<ul class="list-inline">
              <li class="list-inline-item">
                <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                  <i class="fa fa-skype"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                  <i class="fa fa-google"></i>
                </a>
              </li>
            </ul>*/

/* ITEM ANTIGO
  var item = `< div class="card" >
              <div class="card-body">
                <div class="d-flex flex-column align-items-center text-center">
                  <img src="${foto}" width="150">
                </div>
                  <h3 class="card-title">${nome}</h3>
                  <div class="card-text>
        <p align="justify" class="text-center text-secondary">${titulacao}</p>
                <p align="justify" class="text-center text-secondary">${formacao}</p>
                <p align="justify" class="text-center text-muted font-size-sm">${instituicao}</p>
              </div>
            </div >
  </div >`
*/